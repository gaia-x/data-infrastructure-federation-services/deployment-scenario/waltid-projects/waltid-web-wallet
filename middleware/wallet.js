import {config} from '/config.js'
export default function (context) {
    if(context.store.state.auth.loggedIn) {
        console.log('Loading wallet...')
        // TODO: fetch default chain ??
        const defaultChain =  config.evmDefaultChain  
        return context.$axios.$get('/api/wallet/did/list').then(dids => context.store.dispatch('wallet/initialize', {dids, defaultChain}))
    }
}
  